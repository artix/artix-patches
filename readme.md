# artix-patches

Imports packages from Arch and runs corresponding scripts on them. Patches can be re-used and combined with other patches. Never forget to remove systemd again!

## How to use

`artixpkg repo import` clones this repo and automatically applies the patches supplied here. Any executable file in the directoy with the same name as the package being import is executed. Additionally, any `.diff` files will also be applied to the git repo.

## How to add patches

Simply make a directory with the same name as the package you want to apply patches to and add your files there. If you are doing something that can likely be used in multiple packages, check the `_common` directory to see if the patch you want already exists and symlink it where neccessary. Scripts can also read any environment variables defined in a `env.txt` in the directory. The format is a simple `key=value` separated by newlines for each variable. Note that patches are applied in alphabetical order.

See the scripts in `_common` for examples.
