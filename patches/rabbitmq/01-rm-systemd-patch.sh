#!/bin/bash

# rm-systemd-patch.sh
# Indiscriminately removes all lines that contain
# "rabbitmq-customize-systemd-service.patch". And removes the patch file.

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/rabbitmq-customize-systemd-service.patch/d' "${arg}/PKGBUILD"
	rm "rabbitmq-customize-systemd-service.patch"
    done
fi
