#!/bin/bash

# libsystemd-to-liblogind.sh
# replace `libsystemd` with `libelogind`

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/libsystemd.so/libelogind.so/" \
            -i "${arg}/PKGBUILD"
    done
fi
