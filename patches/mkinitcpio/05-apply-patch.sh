#!/bin/sh

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        prepare=$(grep -n "patch -Np1" "${arg}/PKGBUILD" | cut -d : -f 1)
        prepare=$((prepare+1))
        sed -i "${prepare}"'i\  patch -Np1 -i ../0001-no-systemd.patch' "${arg}/PKGBUILD"
    done
fi
