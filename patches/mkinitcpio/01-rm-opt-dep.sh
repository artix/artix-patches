#!/bin/sh

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/systemd-ukify/d' "${arg}/PKGBUILD"
        sed -i "s/NFS'/NFS')/" "${arg}/PKGBUILD"
    done
fi
