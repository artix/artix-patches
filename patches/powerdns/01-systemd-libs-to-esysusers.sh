#!/bin/bash

# systemd-libs-to-esysusers.sh
# replace `systemd-libs` and `libsystemd` with `libelogind`

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E "s/systemd-libs|libsystemd/esysusers/g" \
            -i "${arg}/PKGBUILD"
    done
fi
