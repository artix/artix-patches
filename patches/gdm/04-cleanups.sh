#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/archlinux/artixlinux/g' "${arg}/PKGBUILD"
        sed -i 's/module-elogind-login/module-systemd-login/' "${arg}/PKGBUILD"
    done
fi
