#!/bin/bash

write_lines() {
    ed - "${arg}/PKGBUILD" << EOF
${1} c
${2}
.
w
EOF
}

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        source_line=$(grep -n "^source" "${arg}/PKGBUILD" | cut -d : -f 1)
        source_line=$((source_line+2))
        sed -i "${source_line}"'i\  "${pkgname}.install"' "${arg}/PKGBUILD"

        sha_line=$(grep -n "^sha512sums" "${arg}/PKGBUILD" | cut -d : -f 1)
        sed -e '/sha512sums/d' "${arg}/PKGBUILD"
        SHA='sha512sums=("SKIP"
            "SKIP")'
        write_lines "${sha_line}" "${SHA}"

        sha_line=$((sha_line+2))
        sed -i "${sha_line}"'i\install="brightnessctl.install"' "${arg}/PKGBUILD"

        package_line=$(grep -n "^package" "${arg}/PKGBUILD" | cut -d : -f 1)
        package_line=$((package_line+2))
        sed -i "${package_line}"'i\  make UDEVDIR="/usr/lib/udev/rules.d" INSTALL_UDEV_RULES=1 DESTDIR="$pkgdir" install' "${arg}/PKGBUILD"
    done
fi
