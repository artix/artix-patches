#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    soVer=`curl -s https://gitea.artixlinux.org/packages/glib2/raw/branch/master/PKGBUILD | grep -oh '\-[0-9.]\+\.so'`
    for arg in "$@"
    do
        sed -E "s/(\s+)systemd-libs/\1libudev.so\n\1libgio$soVer\n\1libglib$soVer/" \
            -i "${arg}/PKGBUILD"
        sed -i '/systemd/d' "${arg}/PKGBUILD"
    done
fi