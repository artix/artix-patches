#!/bin/bash

# build flag to disable systemd

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "s/WITH_SYSTEMD=ON/WITH_SYSTEMD=OFF/g" \
		    -i "${arg}/PKGBUILD"
	done
fi
