#!/bin/bash

# Remove a comment from the PKGBUILD file

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "/TODO.*upstream.*file/d" \
		    -i "${arg}/PKGBUILD"
	done
fi
