#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e 's|--with-libsystemd|--with-libsystemd=no|' \
            -i "${arg}/PKGBUILD"
        sed -i '/libsystemd\.so/d' "${arg}/PKGBUILD"
    done
fi