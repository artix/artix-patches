#!/bin/bash

for i in $@ ; do
	sed -e '/systemd-libs/d' \
	    -e 's,systemd,elogind,' -i $i/PKGBUILD
	sed -e 's,elogind=disabled,elogind=enabled,' \
	    -e '/systemd/d' \
	    -e '/install=pulseaudio.install/d' \
	    -e '/iautospawn/s,no,yes,' -i $i/PKGBUILD
done
	    

