#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/systemd-sysvcompat/svc-manager/" "${arg}/PKGBUILD"
        sed -i '/\.service/d' "${arg}/PKGBUILD"
        sed -i '/apcupsd-3.14.4-shutdown.patch/d' "${arg}/PKGBUILD"
        sed -i "s/systemd/elogind/" "${arg}/PKGBUILD"
        line_number=$(grep -n "sha256sums=" "${arg}/PKGBUILD" | cut -d : -f 1)
        line_number=$((line_number+2))
        sed -e "${line_number}"'d' "${arg}/PKGBUILD"
        sed -e "${line_number}"'d' "${arg}/PKGBUILD"
        rm apcupsd-3.14.4-shutdown.patch
    done
fi
