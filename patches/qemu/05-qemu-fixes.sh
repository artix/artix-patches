#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/udev ALL/systemd ALL/' "${arg}/PKGBUILD"
        sed -i 's/edk2-aarch64 //' "${arg}/PKGBUILD"
        sed -i 's/edk2-arm //' "${arg}/PKGBUILD"
    done
fi
