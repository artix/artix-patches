#!/bin/bash

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	        exit 1
else
	for arg in "$@"
		do
			sed -e '/libsystemd/d' \
			    -e '/lib32-systemd/d' \
			    -e 's|-D systemdunit=system|-D libsystemd=false|' \
			    -i $arg/PKGBUILD
	done
fi
