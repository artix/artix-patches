#!/bin/bash

# change archlinux.org to artixlinux.org in PKGBUILD

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "s/archlinux.org/artixlinux.org/g" \
		    -i "${arg}/PKGBUILD"
	done
fi
