#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/Dinstall_systemd=true/Dinstall_systemd=false /' "${arg}/PKGBUILD"
        sed -i '/\.target/d' "${arg}/PKGBUILD"
    done
fi
