#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/")/"/' "${arg}/PKGBUILD"
        sed -i "s/')/'/" "${arg}/PKGBUILD"
        source_line=$(grep -n "source=" "${arg}/PKGBUILD" | cut -d : -f 1)
        source_line=$((source_line+1))
        sed -i "${source_line}"'i\        "0001-meson-add-logind-provider-option.patch")' "${arg}/PKGBUILD"

        b2_line=$(grep -n "b2sums=" "${arg}/PKGBUILD" | cut -d : -f 1)
        b2_line=$((b2_line+1))
        sed -i "${b2_line}"'i\        '"'SKIP')" "${arg}/PKGBUILD"

        prepare_line=$(grep -n "prepare()" "${arg}/PKGBUILD" | cut -d : -f 1)
        prepare_line=$((prepare_line+2))
        sed -i "${prepare_line}"'i\  git apply -3 ../0001-meson-add-logind-provider-option.patch' "${arg}/PKGBUILD"

        sed -i 's/$pkgname build/$pkgname build -Dlogind_provider=elogind/' "${arg}/PKGBUILD"
    done
fi
