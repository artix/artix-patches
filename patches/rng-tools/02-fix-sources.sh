#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/ rngd.conf/ rngd.conf)/" "${arg}/PKGBUILD"
    done
fi
