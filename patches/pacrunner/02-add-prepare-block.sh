#!/bin/bash

write_lines() {
    ed - "${arg}/PKGBUILD" << EOF
${1} c
${2}
.
w
EOF
}

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        package_line=$(wc -l "${arg}/PKGBUILD" | cut -d ' ' -f 1)
        package_line=$((package_line-1))
        PACKAGE='package() {
  cd $pkgname
  make DESTDIR="$pkgdir" install
}'
        write_lines "${package_line}" "${PACKAGE}"
    done
fi
