#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed 's/    sysusers.conf/    sysusers.conf)/' -i "${arg}/PKGBUILD"
    done
fi
