#!/bin/bash

for i in $@; do
	sed -e '/libsystemd.so/d' \
	    -e 's|etc|#etc|' \
	    -e '/libsystemd=/ s,true,false,' \
	    -e '/systemdunit/d' \
	    -e '0,/systemd/ s/systemd/udev/' \
	    -i $i/PKGBUILD
done
