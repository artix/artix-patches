#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        # PKGBUILD
        sed -i 's/archlinux/artixlinux/g' "${arg}/PKGBUILD"
        sed -i 's/arch-release/artix-release/g' "${arg}/PKGBUILD"
        sed -i '/env-generator/d' "${arg}/PKGBUILD"
        sed -i 's/@artixlinux/@archlinux/' "${arg}/PKGBUILD"
        sed -i 's/arch\.conf/artix\.conf/' "${arg}/PKGBUILD"
        rm arch*
        rm env-generator

        # mklogos
        sed -i "s|baseurl=.*|baseurl=https://gitea.artixlinux.org/artix/artwork/raw/branch/master/icons/|" "${arg}/mklogos.sh"
        sed -i 's/arch/artix/g' "${arg}/mklogos.sh"

        # nsswitch
        sed -i 's/files systemd/files/' "${arg}/nsswitch.conf"
        sed -i 's/ \[SUCCESS=merge\] systemd//' "${arg}/nsswitch.conf"
        sed -i 's/mymachines.*/files resolve \[\!UNAVAIL=return\] dns/' "${arg}/nsswitch.conf"

        # os-release
        sed -i 's/bbs/forum/' "${arg}/os-release"
        sed -i 's|gitlab.*|bugs.artixlinux.org\/"|' "${arg}/os-release"
        sed -i 's/0;36/38;2;23;147;209/' "${arg}/os-release"
        sed -i 's/arch/artix/g' "${arg}/os-release"
        sed -i 's/Arch/Artix/g' "${arg}/os-release"

        # profile
        sed -i 's/bash\.bashrc/bash\/bashrc/' "${arg}/profile"

        # sysctl
        sed -i '/archlinux/d' "${arg}/sysctl"

        # sysusers
        sed -i 's/arch/artix/' "${arg}/sysusers"

        # tmpfiles
        sed -i 's/arch/artix/' "${arg}/tmpfiles"
    done
fi
