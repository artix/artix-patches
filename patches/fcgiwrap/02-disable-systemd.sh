#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/ --with-systemd//' "${arg}/PKGBUILD"
        sha_line=$(grep -n "^sha256sums" "${arg}/PKGBUILD" | cut -d : -f 1)
        sha_line=$((sha_line+1))
        sed -i "${sha_line}d" "${arg}/PKGBUILD"
        rm link-with-libsystemd-instead-of-libsystemd-daemon.patch
    done
fi
