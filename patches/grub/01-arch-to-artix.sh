#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/Arch/Artix/' "${arg}/grub.default"
        sed -i 's/Arch/Artix/' "${arg}/sbat.csv"
        sed -i 's/arch/artix/' "${arg}/sbat.csv"
        sed -i 's|https://archlinux.org/packages/core/x86_64/grub/|https://gitea.artixlinux.org/packages/grub/|' "${arg}/sbat.csv"
        line_number=$(grep -n "_package_grub-common_and_bios" "${arg}/PKGBUILD" | tail -1 | cut -d : -f 1)
        line_number=$((line_number+1))
        UPDATE_GRUB='
  install -m0755 /dev/stdin "${pkgdir}"/usr/bin/update-grub <<END
#!/bin/sh
grub-mkconfig -o /boot/grub/grub.cfg
END
}
'
        ed - "${arg}/PKGBUILD" << EOF
${line_number} c
${UPDATE_GRUB}
.
w
EOF
    done
fi
