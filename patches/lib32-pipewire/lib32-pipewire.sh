if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"; do
		sed -e '/systemd/d' \
			-e '/udevrulesdir/a \    -D systemd=disabled' \
			-e '/udevrulesdir/a \    -D rlimits-install=false' \
			-e '/\.install/d' \
			-e '/udevrulesdir/a \    -D logind-provider=libelogind' \
			-i $arg/PKGBUILD
	done
fi
