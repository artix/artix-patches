#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else

    for arg in "$@"
    do
        sed -E 's|(-DCOMPILATION_COMMENT=)".*"|\1"Artix Linux"|' \
            -i "${arg}/PKGBUILD"
        sed -E 's/(-DINSTALL_SYSTEMD_UNITDIR=|-DWITH_SYSTEMD=).*$/\1no/' \
            -i "${arg}/PKGBUILD"
        sed -e "s|'systemd' ||" \
            -e "s|'systemd-libs' ||" \
            -i "${arg}/PKGBUILD"
        sed -i '/mariadb@bootstrap\.service\.d/d' "${arg}/PKGBUILD"
        sed -i '/rm -r usr\/share\/mysql\/systemd\//d' "${arg}/PKGBUILD"
        sed -e "s|systemctl restart mariadb\.service && ||" \
            -i "${arg}/mariadb.install"
    done
fi