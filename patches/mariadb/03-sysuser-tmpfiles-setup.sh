#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else

    for arg in "$@"
    do
        sed -E "s|# already installed.*$|# Setup sysuser and tmpfiles\n\
  install -Dm644 "\$srcdir"/mariadb.sysusers.conf usr/lib/sysusers.d/mariadb.conf\n\
  install -Dm644 "\$srcdir"/mariadb.tmpfiles.conf usr/lib/tmpfiles.d/mariadb.conf|" \
            -i "${arg}/PKGBUILD"
    done
fi