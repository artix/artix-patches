#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";
    open(FILE,$file_name);
    my $content = <FILE>;
    close(FILE);
    $content =~ s/(source\s*=\s*[^)]+)\)/$1\n'mariadb.sysusers.conf' 'mariadb.tmpfiles.conf')/g;
    open(FILE,">$file_name");
    print FILE $content;
    close(FILE);
}
