#!/bin/bash

for i in $@; do
	sed -e 's,systemd,elogind,g' \
	    -e '/selinux=disabled/a -D systemd=disabled' \
		-i $i/PKGBUILD
done
