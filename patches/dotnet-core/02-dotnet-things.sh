#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/arch-/artix-/g" \
            -i "${arg}/PKGBUILD" && \
        sed -E 's|(build\(\).*$)|\1\n  export DOTNET_CLI_TELEMETRY_OPTOUT=1|' \
            -i "${arg}/PKGBUILD"
    done
fi

