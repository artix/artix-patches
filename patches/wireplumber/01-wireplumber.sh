#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/elogind=disabled/d' "${arg}/PKGBUILD" && \
        sed -i '/wireplumber\.install/d' "${arg}/PKGBUILD" && \
        rm "${arg}/wireplumber.install"
    done
fi