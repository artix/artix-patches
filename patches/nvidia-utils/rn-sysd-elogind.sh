#!/bin/bash

for i in $@ ; do
	sed -e '/systemd-homed/d' \
	    -e '/systemd-suspend/d' \
	    -e '/systemd/d' \
	    -e '/new power/a\    install -Dm755 systemd/system-sleep/nvidia "${pkgdir}/usr/lib/elogind/system-sleep/nvidia"' \
	    -e '/new power/a\    install -Dm755 systemd/nvidia-sleep.sh "${pkgdir}/usr/bin/nvidia-sleep.sh"' \
	    -i $i/PKGBUILD
done	
