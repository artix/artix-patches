#!/bin/bash

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -i 's|udevsystemunitdir=${prefix}/lib/systemd/system|systemdsystemunitdir=no|' "${arg}/PKGBUILD"
	done
fi
