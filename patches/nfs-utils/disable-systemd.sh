#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/with-systemd/without-systemd/' "${arg}/PKGBUILD"
        sed -i '/systemd.README/d' "${arg}/PKGBUILD"
        sed -i 's/systemd sysusers/esysusers/' "${arg}/PKGBUILD"
    done
fi
