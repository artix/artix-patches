#!/bin/bash

# Replace any instance of 'Arch Linux' with 'Artix Linux' and 'arch' with 'artix'

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e 's/Arch Linux/Artix Linux/g' \
	    -e "s/\efi_sbat_distro_id='arch'/efi_sbat_distro_id='artix'/g" \
	    -i "${arg}/PKGBUILD"
    done
fi
