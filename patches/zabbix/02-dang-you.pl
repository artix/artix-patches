#!/usr/bin/perl

# remove services that are on the SAME LINE as other files

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";
    open(FILE,$file_name);
    my $content = <FILE>;
    close(FILE);
    $content =~ s/zabbix[^\s]*\.service //g;
    open(FILE,">$file_name");
    print FILE $content;
    close(FILE);
}
