#!/bin/bash

# removes service files that are included via globbing

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i -E "s/service,|,service//g" "${arg}/PKGBUILD"
    done
fi
