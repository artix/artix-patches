#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "/'synapse\.target'/Id" \
            -e "s/'override-hardened\.conf'//g" \
	    -i "${arg}/PKGBUILD"

	rm "${arg}/override-hardened.conf" \
	   "${arg}/synapse.target"
    done
fi
