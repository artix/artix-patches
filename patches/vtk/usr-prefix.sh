#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        search_string='cmake -B build -S ${pkgname^^}-${pkgver} \\'
        append_string='    -DCMAKE_PREFIX_PATH=/usr \\'

        line_number=$(grep -n "$search_string" "${arg}/PKGBUILD" | cut -d':' -f1)

        if [ -z "$line_number" ]; then
            echo "Error: Search string not found in file '${arg}/PKGBUILD'"
            exit 1
        fi

        sed -i "${line_number}s|${search_string}|&\n${append_string}|" "${arg}/PKGBUILD"
    done
fi