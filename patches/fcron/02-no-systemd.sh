#!/bin/bash

# Remove any 'systemd' or 'systemd-libs' within a dependencies or similar array. Does
# not remove newlines but tries to remove surrounding quotes and spaces.

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/systemctl/d' "${arg}/PKGBUILD"
        sed -E "s/--with-systemdsystemunitdir=[^ ]* /--with-systemdsystemunitdir=no /g" \
            -i "${arg}/PKGBUILD"
    done
fi
