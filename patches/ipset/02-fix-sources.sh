#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/tag=v${pkgver}"/tag=v${pkgver}")/' "${arg}/PKGBUILD"
    done
fi
