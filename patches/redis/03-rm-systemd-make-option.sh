#!/bin/bash

# rm-systemd-make-option.sh
# removes the systemd make option from the PKGBUILD file

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/USE_SYSTEMD=yes/d' "${arg}/PKGBUILD"
    done
fi
