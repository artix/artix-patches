if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/artix-meson/ s|build|-D systemd_units=false build|' "${arg}/PKGBUILD"
    done
fi
