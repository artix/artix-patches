#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/SYSTEMD=ON/SYSTEMD=OFF/" \
            -e "s/SYSTEMD=1/SYSTEMD=0/" \
            -e "s/ 'systemd-libs'//" \
            -i "${arg}/PKGBUILD"
    done
fi
