#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/-D b_lto=false/-D b_lto=false -D systemd=false/" "${arg}/PKGBUILD"
    done
fi
