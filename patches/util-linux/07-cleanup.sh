#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/-Dlibutempter=disabled/-Dlibutempter=enabled/' "${arg}/PKGBUILD"
        sed -i 's/udev-sysusers/esysusers/' "${arg}/PKGBUILD"
        sed -i "s/'bash-completion'/'bash-completion'\n             'cryptsetup'/" "${arg}/PKGBUILD"
        sed -i "s|sed|git apply ../0001-util-linux-tmpfiles.patch; sed|" "${arg}/PKGBUILD"
    done
fi

