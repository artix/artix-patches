#!/bin/bash

# Remove package dependencies that are not needed

# 1. python-pytorch: out of repo dependency

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "/python-pytorch/d" \
            -i "${arg}/PKGBUILD"
    done
fi
