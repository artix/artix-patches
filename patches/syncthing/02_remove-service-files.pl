#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";
    open(FILE,$file_name);
    my $content = <FILE>;
    close(FILE);
    $content =~ s/\n[^\n#]*.service//g;
    $content =~ s/(sha256sums([^\n]+\n){6})([^\n]+\n){2}/$1/g;
    open(FILE,">$file_name");
    print FILE $content;
    close(FILE);
}
