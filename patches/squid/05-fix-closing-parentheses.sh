#!/bin/bash

# add closing parentheses to the end of the 'squid.sysusers' line

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "/^\s\+'squid.sysusers'/ s/$/)/" \
		    -i "${arg}/PKGBUILD"
	done
fi
