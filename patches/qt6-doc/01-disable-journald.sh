#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/-DQT_FEATURE_journald=ON/-DQT_FEATURE_journald=OFF/g' "${arg}/PKGBUILD"
    done
fi
