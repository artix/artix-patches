#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i -e 's/\(-DBUILD_JAVA=TRUE\)/-DINSTALL_SYSTEMD_UNITS=OFF \\\n    \1/' "${arg}/PKGBUILD"
    done
fi