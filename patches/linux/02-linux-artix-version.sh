#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        ver=$(grep -r "pkgver=" PKGBUILD | cut -d = -f 2)
        ver=${ver%.*}
        rel=$(grep -r "pkgver=" PKGBUILD | cut -d h -f 2)
        
        artix_ver="
_ver=${ver}
_rel=${rel}
_arch=arch\${_rel}
_artix=\${_arch/arch/artix}
"
        ed - "${arg}/PKGBUILD" << EOF
2 c
${artix_ver}
.
w
EOF
        sed -i 's/^pkgver=.*/pkgver=${_ver}.${_artix}/' "${arg}/PKGBUILD"
        sed -i 's/_srcname=linux-${pkgver%.*}/_srcname=linux-${_ver}/' "${arg}/PKGBUILD"
        sed -i 's/_srctag=v${pkgver%\.\*}-${pkgver##\*\.}/_srctag=v${_ver}-${_arch}/' "${arg}/PKGBUILD"
        sed -i 's/kernel\/v${pkgver%%\.\*}/kernel\/v${_ver%%\.\*}/' "${arg}/PKGBUILD"
        sed -i -r 's/-arch/-artix/g' "${arg}/config"
        sed -i -r 's/modules-arch/modules-artix/g' "${arg}/PKGBUILD"
        sed -i -r 's/wireguard-arch/wireguard-artix/g' "${arg}/PKGBUILD"

        prepare_line=$(grep -n 'echo "Setting config' "${arg}/PKGBUILD" | cut -d : -f 1)
        prepare_line=$((prepare_line-1))
        sed -i "${prepare_line}"'i\  sed -i -r "s/EXTRAVERSION = -${_arch}/EXTRAVERSION = -${_artix}/" "Makefile"' "${arg}/PKGBUILD"
    done
fi
