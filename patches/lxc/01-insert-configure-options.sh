#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        # sed -E "s/^(\s*)(artix-meson)/\1local options=(\n\1\1-Dinit-script=sysvinit\n\1)\n\1\2/g" \
        #     -i "${arg}/PKGBUILD"
        sed -E "s/(artix-meson build)/\1 -Dinit-script=sysvinit/g" \
            -i "${arg}/PKGBUILD"
    done
fi
