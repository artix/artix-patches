#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E "s/(.service['\"]?)\s*\)/\1\n)/g" \
            -i "${arg}/PKGBUILD"
    done
fi
