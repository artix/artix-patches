#!/bin/bash

# remove any instance of '{,.asc}'

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e 's/{,.asc}//g' \
	    -i "${arg}/PKGBUILD"
    done
fi
