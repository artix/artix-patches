#!/bin/bash

# rm-service-socket.sh
# Indiscriminately removes all lines that contain "service,socket"

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/service,socket/d' "${arg}/PKGBUILD"
    done
fi
