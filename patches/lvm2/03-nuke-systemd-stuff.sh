#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's|udevsystemunitdir=/usr/lib/systemd/system|systemdsystemunitdir=no|' "${arg}/PKGBUILD"
        sed -i 's/non-udev/non-systemd/' "${arg}/PKGBUILD"
        start=$(grep -n "remove install section" "${arg}/PKGBUILD" | cut -d : -f 1)
        finish=$((start+4))
        sed -i "${start},${finish}"'d' "${arg}/PKGBUILD"
        start=$(grep -n "Install dmeventd socket and service" "${arg}/PKGBUILD" | cut -d : -f 1)
        finish=$((start+4))
        sed -i "${start},${finish}"'d' "${arg}/PKGBUILD"
        start=$(grep -n "udev support" "${arg}/PKGBUILD" | cut -d : -f 1)
        finish=$((start+6))
        sed -i "${start},${finish}"'d' "${arg}/PKGBUILD"
    done
fi
