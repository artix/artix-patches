#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else

    for arg in "$@"
    do
        sed -E 's/(-DWITH_SYSTEMD=)1/\10/' \
            -i "${arg}/PKGBUILD"
    done
fi