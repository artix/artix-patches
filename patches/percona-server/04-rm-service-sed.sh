#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/PrivateTmp=/d' "${arg}/PKGBUILD"
        sed -i '/\.service/d' "${arg}/PKGBUILD"
    done
fi