#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";

    open(my $input, '<', $file_name) or die("Could not open file '$file_name' $!\n");
    my @lines = <$input>;
    close($input);

    my $section_start = -1;
    my $section_end = -1;
    my $in_for_loop = 0;

    foreach my $i (0..$#lines) {
        if ($in_for_loop > 0) {
            if ($lines[$i] =~ /\{/) {
                $in_for_loop++;
            }
            if ($lines[$i] =~ /\}/ or $lines[$i] =~ /done/) {
                $in_for_loop--;
            }
            if ($in_for_loop == 0) {
                $section_end = $i + 1;
                last;
            }
        }
        if ($lines[$i] =~ /^\s*for[^\n]*\.service/) {
            $section_start = $i;
            $in_for_loop = 1;
        }
    }

    if ($section_end >= 0) {
        splice(@lines, $section_start, $section_end - $section_start);
    } else {
        die("Error: Unable to find service for loop.\n");
    }

    open(my $output, '>', $file_name) or die("Could not open file '$file_name' for writing $!\n");
    print $output @lines;
    close($output);
}