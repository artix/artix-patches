#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/bash\.bash/bash\/bash/" \
            -i "${arg}/PKGBUILD"
        sed -e "s/bash\.bash/bash\/bash/" \
            -i "${arg}/system.bash_logout"
        sed -e "s/bash\.bash/bash\/bash/" \
            -i "${arg}/system.bashrc"
    done
fi

