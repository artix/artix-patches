#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E 's|(^[^\[#])|# \1|' \
            -i "${arg}/dot.bashrc"
    done
fi
