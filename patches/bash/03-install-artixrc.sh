#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E 's|( *)(# *system-wide.*)|\1\2\n\1install -Dm644 artix.bashrc "$pkgdir/etc/bash/bashrc.d/artix.bashrc"|' \
            -i "${arg}/PKGBUILD"
        sed -E 's|( *if.*bash_completion)|for sh in /etc/bash/bashrc.d/*.bashrc ; do\
    if [[ -r ${sh} ]]; then\
        . "${sh}"\
    fi\
done\n\n\1|' \
            -i "${arg}/system.bashrc"
    done
fi