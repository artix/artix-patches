#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/systemd-libs/esysusers/" \
            -e "s/--enable-systemd/--disable-systemd/" \
            -e "s/'systemd'//" \
            -i "${arg}/PKGBUILD"
    done
fi