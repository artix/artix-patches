#!/bin/bash

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"; do
		sed -e '/pkgver()/,+2d' \
			-i "$arg"/PKGBUILD
	done
fi
