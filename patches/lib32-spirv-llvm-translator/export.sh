#!/bin/bash

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"; do
		sed -e '/build()/a\  export CMAKE_PREFIX_PATH=/usr' \
			-e '/build()/a\  export CMAKE_INSTALL_LIBDIR=/usr/lib32' \
			-i "$arg"/PKGBUILD
	done
fi
