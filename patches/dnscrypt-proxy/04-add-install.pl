#!/usr/bin/perl

# add-package.pl
# Appends the contents of PACKAGE_APPEND env variable to the end of the package function.
# Can be configured to target a different function with PACKAGE_BLOCK.
# \n in PACKAGE_APPEND are interpreted as newlines.
# Setting PACKAGE_NO_NEWLINE will disable appending any additional newlines before
# the append_var.

use strict;
use warnings;

if (@ARGV == 0 || ! $ENV{'PACKAGE_APPEND'}) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";

    open(my $input, '<', $file_name) or die("Could not open file '$file_name' $!\n");
    my $content = <$input>;
    close($input);

    my $package_block = $ENV{'PACKAGE_BLOCK'} // 'package';
    my $append_var = $ENV{'PACKAGE_APPEND'};
    my $extra_newline = $ENV{'PACKAGE_NO_NEWLINE'} ? "" : "\n";
    $append_var =~ s/\\n/\n/g; # change \n to actual newlines

    if ($content =~ /(
${package_block}\s*\(\)\s*\{)/) {
        $content =~ s/(
${package_block}\s*\(\)\s*\{[\s\S]*?
)(\})/$1${extra_newline}${append_var}\n}/;
    } else {
        die("Function '${package_block}' not found in '$file_name'.\n");
    }

    open($input, ">", $file_name) or die("Could not write to file '$file_name' $!\n");
    print $input $content;
    close($input);
}
