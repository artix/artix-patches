#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/libldm systemd-sysvcompat openssh qemu/libldm openssh qemu/' "${arg}/PKGBUILD"
        sed -i '/systemd-sysvcompat/d' "${arg}/PKGBUILD"
    done
fi
