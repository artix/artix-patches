#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "/dbus-daemon-units/d" "${arg}/PKGBUILD"
        sed -i "/dbus-reload/d" "${arg}/PKGBUILD"
        sed -i "/unit/d" "${arg}/PKGBUILD"
        sed -i "/hook/d" "${arg}/PKGBUILD"
        sed -i "s/x11_autolaunch=disabled/x11_autolaunch=enabled/" "${arg}/PKGBUILD"
        rm "${arg}/dbus-reload.hook"
    done
fi
