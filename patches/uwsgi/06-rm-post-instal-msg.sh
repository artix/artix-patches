#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
	rm "${arg}/uwsgi.install"
        sed -i '/install=uwsgi\.install/Id' "${arg}/PKGBUILD"
    done
fi
