#!/bin/bash

# remove systemd from archlinux.ini file

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		# remove systemd from archlinux.ini file
		sed -e "s/systemd_logger,//g" \
		    -i "${arg}/archlinux.ini"
		done
fi
