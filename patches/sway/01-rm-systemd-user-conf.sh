#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/systemd-user/d' "${arg}/PKGBUILD"
        sed -i "/'SKIP'/d" "${arg}/PKGBUILD"
        rm "${arg}/50-systemd-user.conf"
        sed -i 's/wayland-protocols/wayland-protocols libelogind/' "${arg}/PKGBUILD"
    done
fi
