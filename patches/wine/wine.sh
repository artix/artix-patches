if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
	    sed -e 's|mingw-w64-gcc||' \
		-e 's|-ffat-lto-objects|-ffat-lto-objects -fPIC|' \
	    -i	$arg/PKGBUILD
    done
    
    for arg in "$@"
    do
	    sed -e '/systemctl/d' \
	        -e '/post_install/a \   echo "binfmt binary formats will be updated at reboot"' \
	    -i	$arg/wine.install
    done
fi
