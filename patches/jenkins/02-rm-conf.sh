#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/conf.d/d' "${arg}/PKGBUILD"
        sed -i "/'jenkins.conf'/d" "${arg}/PKGBUILD"
        rm jenkins.conf
    done
fi
