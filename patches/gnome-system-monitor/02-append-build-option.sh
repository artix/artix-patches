#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/pkgname build/pkgname build -Dsystemd=false/' "${arg}/PKGBUILD"
    done
fi
