#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/pacman-mirrorlist/artix-mirrorlist/" "${arg}/PKGBUILD"
        sed -i "s/Arch/Artix/" "${arg}/PKGBUILD"
        begin=$(grep -n 'install -dm755 "$wantsdir"' "${arg}/PKGBUILD" | cut -d : -f 1)
        finish=$((begin+5))
        sed -i "${begin},${finish}"'d' "${arg}/PKGBUILD"
        sed -i "s/purge debug/purge !debug/" "${arg}/makepkg.conf"
        git -C "${arg}" checkout -- pacman.conf
    done
fi
