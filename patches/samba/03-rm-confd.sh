#!/bin/bash

# rm-confd.sh
# Indiscriminately removes all lines that contain "/conf.d/"

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
	sed -i 's/etc\/conf\.d.*)/)/' "${arg}/PKGBUILD"
	sed -i '/\/conf\.d/d' "${arg}/PKGBUILD"
    done
fi
