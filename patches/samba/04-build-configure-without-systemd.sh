#!/bin/bash

# configure without systemd

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"; do
        sed -i '0,/--with-\\/s//--without-systemd \\/' "${arg}/PKGBUILD"
    done
fi
