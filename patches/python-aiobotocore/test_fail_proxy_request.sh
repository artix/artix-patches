#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E "s/(pytest -m moto tests)/\1 -k \"not test_fail_proxy_request\"/" \
            -i "${arg}/PKGBUILD"
    done
fi