#!/usr/bin/perl

use strict;
use warnings;
use Digest::SHA;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

sub calculate_sha256_checksum {
    my ($filename) = @_;

    open my $file, '<', $filename or die "Cannot open file: $!";
    binmode $file;

    my $sha256 = Digest::SHA->new(256);
    $sha256->addfile($file);

    close $file;

    return $sha256->hexdigest;
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";
    my $tmpfile = $_ . "/vnstatd.tmpfile";
    my $sysusers = $_ . "/vnstatd.sysusers";
    my $tmpfilechecksum = calculate_sha256_checksum($tmpfile);
    my $sysuserschecksum = calculate_sha256_checksum($sysusers);
    open(FILE,$file_name);
    my $content = <FILE>;
    close(FILE);
    $content =~ s/(source\s*=\s*[^)]+)\)/$1\nvnstatd.tmpfile vnstatd.sysusers)/g;
    $content =~ s/(sha256sums\s*=\s*[^)]+)\)/$1\n'$tmpfilechecksum'\n'$sysuserschecksum')/g;
    open(FILE,">$file_name");
    print FILE $content;
    close(FILE);
}
