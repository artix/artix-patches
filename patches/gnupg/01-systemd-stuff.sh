#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/socket/d' "${arg}/PKGBUILD"
        sed -i '/service/d' "${arg}/PKGBUILD"
        sed -i '/systemdir/d' "${arg}/PKGBUILD"
        sed -i '/wantsdir/d' "${arg}/PKGBUILD"
        start=$(grep -n "local unit" "${arg}/PKGBUILD" | cut -d : -f 1)
        finish=$((start+3))
        sed -i "${start},${finish}"'d' "${arg}/PKGBUILD"
    done
fi
