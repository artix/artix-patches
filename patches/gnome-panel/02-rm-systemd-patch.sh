#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        source_line=$(grep -n "source=" "${arg}/PKGBUILD" | cut -d : -f 1)
        source_line=$((source_line+1))
        sed -i "${source_line}"'i\        '"'0001-build-remove-systemd-dependency-and-code.patch'" "${arg}/PKGBUILD"

        prepare_line=$(grep -n "autoreconf" "${arg}/PKGBUILD" | cut -d : -f 1)
        prepare_line=$((prepare_line))
        sed -i "${prepare_line}"'i\  git apply -3 ../0001-build-remove-systemd-dependency-and-code.patch' "${arg}/PKGBUILD"
    done
fi
