#!/bin/bash

# pass commands that may fail to execute or give errors

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "/meson.test.-C.build/s/$/ || :/" \
		    -e "/rm.*usr.lib.modules-load.d.fwupd-msr.conf/s/$/ || :/" \
		    -e "/rmdir.*usr.lib.modules-load.d/s/$/ || :/" \
		    -i "${arg}/PKGBUILD"
	done
fi
