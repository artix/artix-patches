#!/bin/bash

# systemd-to-udev.sh
# replace `systemd` with `udev`

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/systemd/udev/" \
            -i "${arg}/PKGBUILD"
    done
fi

