#!/bin/bash

# force _FORTIFY_SOURCE=2 in CFLAGS/CXXFLAGS in the build() block

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        line_number=$(grep -n "build()" "${arg}/PKGBUILD" | cut -d : -f 1)
        line_number=$((line_number+1))
        sed -i "${line_number}"'i\  export CFLAGS="${CFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"' "${arg}/PKGBUILD"
        line_number=$((line_number+1))
        sed -i "${line_number}"'i\  export CXXFLAGS="${CXXFLAGS/_FORTIFY_SOURCE=3/_FORTIFY_SOURCE=2}"' "${arg}/PKGBUILD"
    done
fi
