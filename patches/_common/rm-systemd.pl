#!/usr/bin/perl

# rm-systemd.pl
# Appends `rm -r $pkgdir/usr/lib/systemd` or custom REMOVE_VAR to end of
# package block.
# Package block can be customized using the PACKAGE_BLOCK env variable.
# Multiple blocks can be specified (comma-delineated).

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";

    open(my $input, '<', $file_name) or die("Could not open file '$file_name' $!\n");
    my @lines = <$input>;
    close($input);

    my @package_blocks = split(",", $ENV{'PACKAGE_BLOCK'} // 'package');
    my $remove_var = $ENV{'REMOVE_VAR'} // "rm -r \$pkgdir/usr/lib/systemd";

    foreach my $package_block (@package_blocks) {
        my $end_index = 0;
        my $in_package_section = 0;
        my $line_prefix = "";
        foreach my $i (0..$#lines) {
            if ($in_package_section > 0) {
                if ($lines[$i] =~ /\{/) {
                    $in_package_section++;
                }
                if ($lines[$i] =~ /\}/) {
                    $in_package_section--;
                }
                if ($in_package_section == 0) {
                    $end_index = $i;
                    last;
                }
                elsif ($in_package_section == 1 && length($line_prefix) == 0 && $lines[$i] =~ /^(\s+)/) {
                    $line_prefix = $1;
                }
            }
            if ($lines[$i] =~ /^\s*$package_block\(\)/) {
                $in_package_section = 1;
            }
        }

        if ($end_index != 0) {
            splice(@lines, $end_index, 0, "\n" . $line_prefix . $remove_var . "\n");
        } else {
            die("Error: Unable to find package section in PKGBUILD.\n");
        }
    }

    open(my $output, '>', $file_name) or die("Could not open file '$file_name' for writing $!\n");
    print $output @lines;
    close($output);
}
