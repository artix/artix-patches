#!/bin/bash

# 01-add-json-glib-makedep.sh
# replace 'intltool' with 'intltool json-glib' for makedepends

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/intltool/intltool json-glib/" \
            -i "${arg}/PKGBUILD"
    done
fi
