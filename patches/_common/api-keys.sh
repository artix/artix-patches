#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e '/api_key=.*$/d' \
        -e '/api-key=.*$/d' \
        -e '/api-keyfile=/d' \
        -e '/^#.*[Kk]eys/d' \
        -e '/^#.*Arch.*ONLY/d' \
        -e '/^#.*more info/d' -i "${arg}/PKGBUILD"
    done
fi
