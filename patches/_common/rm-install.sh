#!/bin/bash

# rm-install.sh
# Indiscriminately removes all .install files.

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/install=/d' "${arg}/PKGBUILD"
        rm ${arg}/*.install
    done
fi
