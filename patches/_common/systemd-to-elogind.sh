#!/bin/bash

# systemd-to-elogind.sh
# replace `systemd` with `elogind`

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/systemd/elogind/g" \
            -i "${arg}/PKGBUILD"
    done
fi
