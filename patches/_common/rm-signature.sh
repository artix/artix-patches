#!/bin/bash

# Remove signatures from sources

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/{,.sig}//g" \
            -e "s/?signed//g" \
            -i "${arg}/PKGBUILD"
    done
fi
