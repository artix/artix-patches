#!/bin/bash

# Replace any instance of 'Arch Linux' with 'Artix Linux'

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/Arch Linux/Artix Linux/g' "${arg}/PKGBUILD"
    done
fi
