#!/usr/bin/perl

# insert-config-options.pl
# Appends the contents of CONFIG_OPTIONS to the cmake_options block.
# Different config block can be targeted by setting OPTION_VAR

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";

    open(my $input, '<', $file_name) or die("Could not open file '$file_name' $!\n");
    my @lines = <$input>;
    close($input);

    my $end_index = 0;
    my $in_options = 0;
    my $line_prefix = "";
    my $options_var = $ENV{'OPTION_VAR'} // 'cmake_options';
    my $config_options = $ENV{'CONFIG_OPTIONS'} // "";

    foreach my $i (0..$#lines) {
        if ($in_options > 0) {
            if ($lines[$i] =~ /\)/) {
                $in_options--;
            }
            if ($in_options == 0) {
                $end_index = $i;
                last;
            }
            elsif ($in_options == 1 && length($line_prefix) == 0 && $lines[$i] =~ /^(\s+)/) {
                $line_prefix = $1;
            }
        }
        if ($lines[$i] =~ /^\s*(local )?${options_var}\s*=/) {
            $in_options = 1;
        }
    }

    $config_options =~ s/\\n/\n${line_prefix}/g;

    if ($end_index != 0) {
        splice(@lines, $end_index, 0, $line_prefix . $config_options . "\n");
    } else {
        die("Error: Unable to find config section in PKGBUILD.\n");
    }

    open(my $output, '>', $file_name) or die("Could not open file '$file_name' for writing $!\n");
    print $output @lines;
    close($output);
}
