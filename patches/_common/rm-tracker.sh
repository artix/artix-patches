#!/bin/sh

# remove tracker3 dependencies and build options

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/tracker/d' "${arg}/PKGBUILD"
    done
fi
