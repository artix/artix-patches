#!/usr/bin/perl

# skip-systemd-install.pl
# removes install commands that contain `.service`

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";
    open(FILE,$file_name);
    my $content = <FILE>;
    close(FILE);
    my $newlines = "";
    while ($content =~ s/([^\\]\n)[^\n#]*install([^\n]|\\\n)*\.service['"]{0,1}\s*\n/$1${newlines}/g) {
        $newlines .= "\n";
    }
    open(FILE,">$file_name");
    print FILE $content;
    close(FILE);
}
