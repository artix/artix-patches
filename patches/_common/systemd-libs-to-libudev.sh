#!/bin/bash

# systemd-libs-to-udev.sh
# replace `systemd-libs` with `libudev`

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/systemd-libs/libudev/" \
            -i "${arg}/PKGBUILD"
    done
fi

