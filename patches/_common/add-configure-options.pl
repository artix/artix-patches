#!/usr/bin/perl

# Inside a local variable containing configure options such as:
# 'local meson_options=(', this script adds additional lines of
# options.
#
# By default, it will look for `meson_options=` and add '-D systemd=false'.
# \n in CONFIG_OPTIONS are interpreted as newlines

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";
    open(FILE,$file_name);
    my $content = <FILE>;
    close(FILE);
    my $option = $ENV{'CONFIG_OPTIONS'} // '-D systemd=false';
    $option =~ s/\\n/\n/g; # change \n to actual newlines
    my $option_var = $ENV{'OPTION_VAR'} // 'meson_options';
    $content =~ s/($option_var=\((\s*))/$1$option$2/g;
    open(FILE,">$file_name");
    print FILE $content;
    close(FILE);
}
