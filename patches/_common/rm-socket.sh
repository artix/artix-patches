#!/bin/bash

# rm-service.sh
# Indiscriminately removes all lines that contain ".socket"

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/\.socket/d' "${arg}/PKGBUILD"
    done
fi
