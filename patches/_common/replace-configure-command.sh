#!/bin/bash

# replace-configure-command.sh
# Replace a configure command within a PKGBUILD.
# Useful for adding additional switches after whatever the generic onfigure is.
# Customize using CONFIG_FIND and CONFIG_REPLACE variables.
# Defaults to replacing "artix-meson" with "artix-meson -Dsystemd=disabled".

CONFIG_FIND="${CONFIG_FIND:-artix-meson}"
CONFIG_REPLACE="${CONFIG_REPLACE:-artix-meson -Dsystemd=disabled}"

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/${CONFIG_FIND}/${CONFIG_REPLACE}/" "${arg}/PKGBUILD"
    done
fi
