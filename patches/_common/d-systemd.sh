#!/bin/bash

# d-systemd.sh
# Indiscriminately removes all lines that contain "systemd"
# Less useful than some of the other scripts

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/systemd/Id' "${arg}/PKGBUILD"
    done
fi