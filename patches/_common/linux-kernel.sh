#!/bin/bash

# linux-kernal.sh
# Sets the hostname in linux PKGBUILDs to `artixlinux`

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        oldsha256=`sha256sum "${arg}/config" | cut -d ' ' -f1`
        oldsha512=`sha512sum "${arg}/config" | cut -d ' ' -f1`
        oldb2sums=`b2sum "${arg}/config" | cut -d ' ' -f1`
        sed -e 's|KBUILD_BUILD_HOST=.*|KBUILD_BUILD_HOST=artixlinux|' -i "${arg}/PKGBUILD"
        sed -e 's|CONFIG_DEFAULT_HOSTNAME=.*|CONFIG_DEFAULT_HOSTNAME="artixlinux"|' \
            -i "${arg}/config"
        newsha256=`sha256sum "${arg}/config" | cut -d ' ' -f1`
        newsha512=`sha512sum "${arg}/config" | cut -d ' ' -f1`
        newb2sums=`b2sum "${arg}/config" | cut -d ' ' -f1`
        sed -e "s|${oldsha256}|${newsha256}|g" \
            -e "s|${oldsha512}|${newsha512}|g" \
            -e "s|${oldb2sums}|${newb2sums}|g" \
            -i "${arg}/PKGBUILD"
    done
fi