#!/bin/bash

# rm-timer.sh
# Indiscriminately removes all lines that contain ".timer"

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/\.timer/d' "${arg}/PKGBUILD"
    done
fi