#!/bin/bash

# rm-systemd-deps.sh
# Remove any 'systemd' or 'systemd-libs' within a dependencies or similar array. Does
# not remove newlines but tries to remove surrounding quotes and spaces.

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    deps=(systemd-libs systemd-tools python-systemd lib32-systemd libsystemd.so libsystemd systemd)

    for arg in "$@"
    do
        for i in "${deps[@]}"
        do
            sed -i -E "s/ \"$i\"|\"$i\" //g" "${arg}/PKGBUILD"
            sed -i -E "s/ '$i'|'$i' //g" "${arg}/PKGBUILD"
            sed -i -E "s/ $i|$i //g" "${arg}/PKGBUILD"
        done
    done
fi
