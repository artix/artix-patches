#!/bin/bash

# linux-arch-version.sh
# Replaces "arch" in the package version with "artix" to avoid confusion

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E "s|^(pkgver=.*\.)(arch)(.*)$|\1artix\3\narch\1\2\3|" -i "${arg}/PKGBUILD"
        sed -E 's|\$\{pkgver|${archpkgver|' -i "${arg}/PKGBUILD"
        sed -E 's|\$pkgver|$archpkgver|' -i "${arg}/PKGBUILD"
    done
fi
