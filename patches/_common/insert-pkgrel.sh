#!/bin/bash

# insert-pkgrel.sh
# adds `-$pkgrel` to the end of the filename for a tarball source.

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E 's|(\.tar\.gz::)|-\$pkgrel\1|' \
            -i "${arg}/PKGBUILD"
    done
fi