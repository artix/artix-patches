#!/usr/bin/perl

# add-source.pl
# Appends the contents of SOURCES env variable to the end of the sources list.
# Can be configured to add to a different section with SOURCE_BLOCK.
# \n in SOURCES are interpreted as newlines
# Setting SOURCES_NO_NEWLINE will disable appending any additional newlines before
# the append_var.

use strict;
use warnings;

if (@ARGV == 0 || ! $ENV{'SOURCES'}) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";

    open(my $input, '<', $file_name) or die("Could not open file '$file_name' $!\n");
    my $content = <$input>;
    close($input);

    my $source_block = $ENV{'SOURCE_BLOCK'} // 'source';
    my $append_var = $ENV{'SOURCES'};
    my $extra_newline = $ENV{'SOURCES_NO_NEWLINE'} ? "" : "\n";
    $append_var =~ s/\\n/\n/g; # change \n to actual newlines

    $content =~ s/([^\\]\n\s*${source_block}=\([^)]+)\)/$1${extra_newline}${append_var})/;

    open($input,">$file_name");
    print $input $content;
    close($input);
}
