#!/bin/bash

for i in $@ ; do
	sed -e 's,systemd,elogind,' \
		-i $i/PKGBUILD
done
