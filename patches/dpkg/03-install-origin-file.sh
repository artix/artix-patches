#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/ archlinux / artixlinux /" \
            -i "${arg}/PKGBUILD"
        sed -E 's|(\s+install -vDm644 "\$srcdir/origin\.)(archlinux)(" "\$pkgdir/etc/dpkg/origins/)(archlinux)"|\1\2\3\4"\n\1artixlinux\3artixlinux"|' \
            -i "${arg}/PKGBUILD"
    done
fi
