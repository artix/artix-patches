#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/'systemd'//" \
            -e "s/'systemd-libs'//" \
            -e "s/libsystemd.so/libudev.so/" \
            -e "s/--enable-systemd/--disable-systemd/" \
            -i "${arg}/PKGBUILD"
    done
fi