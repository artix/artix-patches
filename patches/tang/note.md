- The `01-d-systemd.sh` can be destructive, since it's removing the entire
`checkdepends` line. Beware if some dependency is added.
