#!/bin/bash

# Remove package dependencies that are not needed

# 1. python-keras: needs tensorflow, which is not available in the official

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/ 'python-keras'//g" \
            -i "${arg}/PKGBUILD"
    done
fi
