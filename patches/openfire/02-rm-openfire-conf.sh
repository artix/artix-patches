#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "/'openfire\.conf'/d" "${arg}/PKGBUILD" && \
        sed -e "s|'etc/conf.d/openfire'||" \
            -i "${arg}/PKGBUILD" && \
        sed -i "/\/etc\/conf.d\/openfire/d" "${arg}/PKGBUILD"
    done
fi