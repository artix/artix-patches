#!/bin/bash

# configure without systemd

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"; do
        # Previous scripts make these turn into udev instead of systemd
        sed -i 's/libudev=yes/libsystemd=no/' "${arg}/PKGBUILD"
        sed -i 's|udevsystemunitdir=/usr/lib/systemd/system|systemdsystemunitdir=no|' "${arg}/PKGBUILD"
        sed -i '/udevenvfile/d' "${arg}/PKGBUILD"
    done
fi
