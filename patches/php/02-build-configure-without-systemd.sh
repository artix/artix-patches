#!/bin/bash

# configure without systemd

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "0,/--with-fpm-/s//--without-fpm-systemd /" \
		    -i "${arg}/PKGBUILD"

	done
fi
