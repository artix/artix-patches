#!/bin/bash

# configure without systemd

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "/sendmail_path/s/$/ || :/" \
		    -i "${arg}/PKGBUILD"
	done
fi
