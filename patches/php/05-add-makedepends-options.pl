#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";
    open(my $fh, '<', $file_name) or die "Could not open file '$file_name' $!";
    my $content = <$fh>;
    close($fh);
    $content =~ s/(makedepends=\(\s*.*?)(\))/${1} 'argon2'$2/s;
    open($fh, '>', $file_name) or die "Could not open file '$file_name' $!";
    print $fh $content;
    close($fh);
}
