#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s|-Dsystemdsystemunitdir=/usr/lib/systemd/system|-Dsystemdsystemunitdir=''|" "${arg}/PKGBUILD"
    done
fi
