#!/bin/bash

for i in $@; do
	sed -e '/zshcompletiondir/a -D systemduserunitdir=no' -i $i/PKGBUILD
	sed -e '/systemduserunitdir=no/a -D systemd=disabled' \
	    -e '/lib32-systemd/d' \
	    -i $i/PKGBUILD
done
