#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's|--enable-systemd|--enable-elogind --eloginddir=/usr/lib/elogind|' "${arg}/PKGBUILD"
    done
fi
