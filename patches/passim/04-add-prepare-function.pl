#!/usr/bin/perl

# add-prepare-function.pl
# Inserts a prepare() function before the build() function in PKGBUILD.

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";
    open(FILE, $file_name) or die("Cannot open $file_name: $!");
    my $content = <FILE>;
    close(FILE);

    # Insert prepare() function before build()
    if ($content =~ s/^(\s*)build\(\)\s*{/$1\nprepare() {\n  patch -Np1 -d \$pkgname -i ..\/0001-no-systemd.patch\n}\n$1build() {/m) {
        open(FILE, ">$file_name") or die("Cannot write to $file_name: $!");
        print FILE $content;
        close(FILE);
    }
}
