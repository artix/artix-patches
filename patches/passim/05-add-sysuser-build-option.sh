#!/bin/bash

# add sysusersdir option to build options in PKGBUILD

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"; do
	sed -e "/artix-meson \${pkgname} build/s|$|  -D sysusersdir=/usr/lib/sysusers.d|" \
	    -i "$arg/PKGBUILD"
    done
fi
