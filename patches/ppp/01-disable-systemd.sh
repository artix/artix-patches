#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else

    for arg in "$@"
    do
        sed -i '/\.systemd/d' "${arg}/PKGBUILD"
        sed -i 's/--enable-systemd/--disable-systemd/' "${arg}/PKGBUILD"
        rm "${arg}/ppp.systemd"
    done
fi
