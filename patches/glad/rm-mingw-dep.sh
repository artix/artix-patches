#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/'mingw-w64-gcc' //" "${arg}/PKGBUILD"
    done
fi
