#!/bin/bash

# Remove the service and relevant sum

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "s/'geth-user.service'//" \
		    -i "${arg}/PKGBUILD"
	done
fi
