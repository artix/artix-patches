#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        line_number=$(grep -n "replaces=" "${arg}/PKGBUILD" | cut -d : -f 1)
        line_number=$((line_number + 1))
        sed -i "${line_number}i options=(debug)" "${arg}/PKGBUILD"
    done
fi
