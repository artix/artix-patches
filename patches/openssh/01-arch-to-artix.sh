#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        mv "${arg}/99-archlinux.conf" "${arg}/99-artixlinux.conf"
        sed -i 's/Arch/Artix/' "${arg}/99-artixlinux.conf"
        sed -i 's/99-archlinux.conf/99-artixlinux.conf/' "${arg}/openssh.tmpfiles"
        sed -i 's/99-archlinux.conf/99-artixlinux.conf/' "${arg}/PKGBUILD"
    done
fi
