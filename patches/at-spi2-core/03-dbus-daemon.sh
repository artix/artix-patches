#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/default_bus=dbus-broker/default_bus=dbus-daemon/" "${arg}/PKGBUILD"
    done
fi

