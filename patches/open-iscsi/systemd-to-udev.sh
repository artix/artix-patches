if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
			  sed -e "s,systemd-libs,libudev," \
					  -i ${arg}/PKGBUILD
        sed -e "s/systemd/udev/" \
					  -e "/rulesdir/a\    -Dno_systemd=true" \
            -i "${arg}/PKGBUILD"
    done
fi

