#!/bin/bash

# 02-fix-glib.sh
# replace 'samurai' with 'samurai glib2-devel' for makedepends

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/samurai/samurai glib2-devel/" \
            -i "${arg}/PKGBUILD"
    done
fi
