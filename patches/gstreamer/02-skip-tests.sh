#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/build --print-errorlogs -t 5/build --print-errorlogs -t 5 || :/' "${arg}/PKGBUILD"
    done
fi
