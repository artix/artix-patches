#!/bin/bash

for i in "$@"; do
	sed -e 's|mingw-w64-gcc||' \
	    -e '/mingw32-strip/d' \
	    -e '/depends/i CFLAGS+=" -Wno-error=incompatible-pointer-types -fPIC"' \
	    -i "$i"/PKGBUILD
for arg in "$@"
	    do
		sed -e '/systemctl/d' \
		    -e '/post_install/a \   echo "binfmt binary formats will be updated at reboot"' \
		    -i  $arg/wine.install
	    done

done
