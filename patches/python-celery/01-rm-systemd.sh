#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/^depends=(/depends=('etmpfiles' /" \
            -e "s/'systemd'/'esysusers'/" \
            -e "s/celery@.service//" \
            -i "${arg}/PKGBUILD"
        sed -i '/systemd/d' "${arg}/PKGBUILD"
    done
fi