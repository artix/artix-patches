#!/bin/bash

# updpkgsums.sh
# runs updpkgsums and then runs `git clean -fdx`
# useful if Arch checksums aren't matching what the build server is getting
# or if sources are added/removed by patches.

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        cd "${arg}"
        git add -A && \
        updpkgsums && \
        git clean -fdx && \
        git reset
        cd -
    done
fi