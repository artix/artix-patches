#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/lto)/lto debug)/" "${arg}/PKGBUILD"
        sed -i "s/bugurl.*gitlab\.archlinux.*/bugurl=https:\/\/gitea\.artixlinux\.org\/packages\/glibc\/issues/" "${arg}/PKGBUILD"
    done
fi
