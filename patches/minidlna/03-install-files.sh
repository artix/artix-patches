#!/bin/sh

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        line_number=$(grep -n "DESTDIR=" "${arg}/PKGBUILD" | cut -d : -f 1)
        line_number=$((line_number+1))
        SYSUSERS='
  install -Dm0644 "$srcdir"/minidlna.sysusers "$pkgdir"/usr/lib/sysusers.d/minidlna.conf
  install -Dm0644 "$srcdir"/minidlna.tmpfiles "$pkgdir"/usr/lib/tmpfiles.d/minidlna.conf
'
        ed - "${arg}/PKGBUILD" << EOF
${line_number} c
${SYSUSERS}
.
w
EOF
    done
fi
