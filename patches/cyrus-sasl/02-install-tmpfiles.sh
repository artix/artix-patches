#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/saslauthd.conf.d/tmpfiles.conf/' "${arg}/PKGBUILD"
        sed -i 's|etc/conf.d/saslauthd|usr/lib/tmpfiles.d/saslauthd.conf|' "${arg}/PKGBUILD"
        rm saslauthd.conf.d
    done
fi
