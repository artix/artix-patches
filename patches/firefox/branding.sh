#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e '/archlinux.org/! s|archlinux|artixlinux|g' -i "${arg}/PKGBUILD"
        sed -E 's|(about=Mozilla Firefox.*) for .*$|\1 for Artix Linux|' \
            -i "${arg}/PKGBUILD"
    done
fi