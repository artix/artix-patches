#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sha_line=$(grep -n "^sha512sums" "${arg}/PKGBUILD" | cut -d : -f 1)
        sha_line=$((sha_line+2))
        sed -i "${sha_line}d" "${arg}/PKGBUILD"

        b2_line=$(grep -n "^b2sums" "${arg}/PKGBUILD" | cut -d : -f 1)
        b2_line=$((b2_line+2))
        sed -i "${b2_line}d" "${arg}/PKGBUILD"
    done
fi
