#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/ccache)/ccache debug)/" "${arg}/PKGBUILD"
        sed -i "s|gitlab\.archlinux.*|gitea\.artixlinux\.org\/packages\/binutils\/issues \\\|" "${arg}/PKGBUILD"
    done
fi
