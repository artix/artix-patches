#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/\.path/d' "${arg}/PKGBUILD"
        sed -i "s/gpm.sh'/gpm.sh')/" "${arg}/PKGBUILD"
        rm gpm.path
    done
fi
