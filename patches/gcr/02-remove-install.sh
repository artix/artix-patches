#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "/install=/d" "${arg}/PKGBUILD"
        rm "${arg}/gcr.install"
    done
fi
