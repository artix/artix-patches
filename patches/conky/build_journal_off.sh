#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/systemd/d' "${arg}/PKGBUILD"
        sed -e 's/BUILD_JOURNAL=ON/BUILD_JOURNAL=OFF/' \
            -i "${arg}/PKGBUILD"
    done
fi