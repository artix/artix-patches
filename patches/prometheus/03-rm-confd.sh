#!/bin/bash

# rm-confd.sh
# Indiscriminately removes all lines that contain "/conf.d/" and related lines

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "s/ 'etc\/conf\.d\/prometheus'//" \
		    -e "s/prometheus\.conf)/)/" \
		    -e "/install.*etc\/conf\.d/d" \
		    -i "${arg}/PKGBUILD"
	done
fi
