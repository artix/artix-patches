#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/GODEBUG=x509sha1=1 go test -short .\/.../GODEBUG=x509sha1=1 go test -short .\/... || :/' "${arg}/PKGBUILD"
    done
fi
