#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/'systemd'/'elogind' 'libuv'/" \
            -i "${arg}/PKGBUILD"
        line_number=$(grep -n "cmake -GNinja" "${arg}/PKGBUILD" | cut -d : -f 1)
        line_number=$((line_number + 1))
        sed -i "${line_number}"'i\        -DUSE_SYSTEMD=off \\' "${arg}/PKGBUILD"
    done
fi
