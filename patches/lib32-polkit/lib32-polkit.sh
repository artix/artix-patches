#!/bin/bash

for i in $@; do
	sed -e '/cross-file/a\    -D session_tracking=elogind' \
	    -e '/systemd/d' \
	    -e '/makedepends/a\  libelogind' \
	    -i "$i"/PKGBUILD
done
