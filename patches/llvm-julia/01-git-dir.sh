#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e 's|git+https://github.com/JuliaLang/llvm-project|llvm-project-j::git+https://github.com/JuliaLang/llvm-project|' \
            -e 's|llvm-project/llvm|llvm-project-j/llvm|' -i "${arg}/PKGBUILD"
    done
fi