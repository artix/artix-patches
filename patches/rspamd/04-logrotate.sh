#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e 's|/usr/bin/systemctl --signal USR1 kill rspamd.service|kill -HUP $(pidof rspamd)|g' \
            -i "${arg}/rspamd.logrotate"
    done
fi