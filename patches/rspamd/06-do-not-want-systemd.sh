#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e 's/-DWANT_SYSTEMD_UNITS=[^\s\\]*/-DWANT_SYSTEMD_UNITS=OFF /g' \
            -i "${arg}/PKGBUILD"
    done
fi