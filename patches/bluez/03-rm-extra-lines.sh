#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i -E "/fakeinstall.*main\.conf/d" "${arg}/PKGBUILD"
        sed -i -E "/fakeinstall.*input\.conf/d" "${arg}/PKGBUILD"
        sed -i -E "/fakeinstall.*network\.conf/d" "${arg}/PKGBUILD"
        sed -i -E "/chmod.*bluetooth/d" "${arg}/PKGBUILD"
        sed -i -E "/make sure there are no files left to install/d" "${arg}/PKGBUILD"
        sed -i -E "/fakeinstall.*libbluetooth\.la/d" "${arg}/PKGBUILD"
        sed -i -E "/fakeinstall.*rmdir/d" "${arg}/PKGBUILD"
    done
fi
