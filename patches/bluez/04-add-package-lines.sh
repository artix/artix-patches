#!/bin/bash

write_lines() {
    ed - "${arg}/PKGBUILD" << EOF
${1} c
${2}
.
w
EOF
}

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        # 4 different blocks to insert
        config_line=$(grep -n "with-dbusconfdir" "${arg}/PKGBUILD" | cut -d : -f 1)
        config_line=$((config_line+1))

        CONFIG='          --with-udevdir=/usr/lib/udev \
          --disable-systemd \
          --enable-btpclient \'
        write_lines "${config_line}" "${CONFIG}"

        bluez_line=$(grep -n "bluetoothd.8" "${arg}/PKGBUILD" | cut -d : -f 1)
        bluez_line=$((bluez_line+2))

        BLUEZ='  # ship upstream main config files
  install -dm555 "${pkgdir}"/etc/bluetooth
  install -Dm644 "${srcdir}"/"${pkgbase}"-${pkgver}/src/main.conf "${pkgdir}"/etc/bluetooth/main.conf
  install -Dm644 "${srcdir}"/"${pkgbase}"-${pkgver}/profiles/input/input.conf "${pkgdir}"/etc/bluetooth/input.conf
  install -Dm644 "${srcdir}"/"${pkgbase}"-${pkgver}/profiles/network/network.conf "${pkgdir}"/etc/bluetooth/network.conf

  # bluetooth.service wants ConfigurationDirectoryMode=0555
  chmod -v 555 "${pkgdir}"/etc/bluetooth
'
        write_lines "${bluez_line}" "${BLUEZ}"

        mesh_line=$(grep -n "bluetooth-meshd.8" "${arg}/PKGBUILD" | cut -d : -f 1)
        mesh_line=$((mesh_line+2))

        MESH='  # ship upstream mesh config file
  install -dm555 "${pkgdir}"/etc/bluetooth
  install -Dm644 "${srcdir}"/"${pkgbase}"-${pkgver}/mesh/mesh-main.conf "${pkgdir}"/etc/bluetooth/mesh-main.conf
}'
        write_lines "${mesh_line}" "${MESH}"

        obex_line=$(grep -n "org.bluez.obex" "${arg}/PKGBUILD" | cut -d : -f 1)
        obex_line=$((obex_line+2))

        OBEX='  # Installs dbus service
  install -Dm644 /dev/stdin "$pkgdir"/usr/share/dbus-1/services/org.bluez.obex.service <<EOF
[D-BUS Service]
Name=org.bluez.obex
Exec=/usr/lib/bluetooth/obexd
EOF
}'
        write_lines "${obex_line}" "${OBEX}"
    done
fi
