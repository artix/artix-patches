#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e 's|gkrellm.service\s*)|)|' \
            -e "s|'[a-f0-9]\{64\}'\s*)|)|" \
            -i "${arg}/PKGBUILD"
        sed -i '/gkrellm\.service/d' "${arg}/PKGBUILD"
    done
fi