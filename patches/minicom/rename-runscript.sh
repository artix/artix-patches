#!/bin/sh

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        line_number=$(grep -n "install -Dm644" "${arg}/PKGBUILD" | cut -d : -f 1)
        line_number=$((line_number+1))
        RENAME='
  # Fix conflict with OpenRC runscript
  mv ${pkgdir}/usr/bin/runscript ${pkgdir}/usr/bin/runscript-minicom
  echo "pu scriptprog       runscript-minicom" >> ${pkgdir}/etc/minirc.dfl
}
'
        ed - "${arg}/PKGBUILD" << EOF
${line_number} c
${RENAME}
.
w
EOF
    done
fi
