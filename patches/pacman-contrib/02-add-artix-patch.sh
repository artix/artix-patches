#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        line_number=$(grep -n 'autogen.sh' "${arg}/PKGBUILD" | cut -d : -f 1)
        sed -i "${line_number}"'i\  patch -Np 1 -i ../pacman-contrib-artix.patch' "${arg}/PKGBUILD"
    done
fi
