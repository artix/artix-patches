#!/bin/sh

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E -e 's/\$pkgname-6.2-systemd_sysusers_groups.patch|\$pkgbase-6.2-systemd_sysusers_groups.patch/0001-brlapi-use-elogind-instead-of-systemd.patch/' \
            -i "${arg}/PKGBUILD"
        line_number=$(grep -n "disable-stripping" "${arg}/PKGBUILD" | cut -d : -f 1)
        line_number=$((line_number+1))
        sed -i "${line_number}"'i\    --without-service-package' "${arg}/PKGBUILD"
        sed -i "/install-systemd/d" "${arg}/PKGBUILD"
        document_number=$(grep -n 'Documents/$pkgbase.conf' "${arg}/PKGBUILD" | cut -d : -f 1)
        document_number=$((document_number+1))
        SYSUSERS='
  # manually install sysusers/tmpfiles files from srcdir
  mkdir -p "$pkgdir/usr/lib/sysusers.d/"
  mkdir -p "$pkgdir/usr/lib/tmpfiles.d/"
  install -vDm 644 Autostart/Systemd/sysusers "$pkgdir/usr/lib/sysusers.d/"
  install -vDm 644 Autostart/Systemd/tmpfiles "$pkgdir/usr/lib/tmpfiles.d/"
'
        ed - "${arg}/PKGBUILD" << EOF
${document_number} c
${SYSUSERS}
.
w
EOF
        rm "${arg}/brltty-6.2-systemd_sysusers_groups.patch"
    done
fi
