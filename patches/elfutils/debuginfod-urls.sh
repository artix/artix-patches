#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/debuginfod.archlinux/debuginfod.artixlinux/' "${arg}/PKGBUILD"
        sed -i 's/debuginfod\/archlinux/debuginfod\/artixlinux/' "${arg}/PKGBUILD"
    done
fi
