#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/modules-arch/modules-artix/" "${arg}/PKGBUILD"
        sed -i "s/Arch Kernel/Artix Kernel/" "${arg}/PKGBUILD"
    done
fi
