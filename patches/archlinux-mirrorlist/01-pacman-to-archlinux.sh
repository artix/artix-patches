#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "s/pacman-mirrorlist/archlinux-mirrorlist/" "${arg}/PKGBUILD"
        sed -i "s|pacman.d/mirrorlist|pacman.d/mirrorlist-arch|" "${arg}/PKGBUILD"
        sed -i 's|$pkgdir/etc/pacman.d/|$pkgdir/etc/pacman.d/mirrorlist-arch|' "${arg}/PKGBUILD"
        sed -i 's|#Server = https://geo|Server = https://geo|' "${arg}/mirrorlist"
    done
fi
