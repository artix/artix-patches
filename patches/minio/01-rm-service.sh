#!/bin/bash

# Remove the service and relevant sum

if [ "$#" -eq 0 ]; then
	echo "Usage: $0 <package repo>"
	exit 1
else
	for arg in "$@"
	do
		sed -e "/minio.service/Id" \
		    -e "/'f4df8e50618712b6e5f62e2674eca4430ef17ef003426bd83ea6b427da4e0fb519589cc14547b08db4b4a0de114488920071295a680b0c1cb5fd508d31576190'/Id" \
		    -i "${arg}/PKGBUILD"

	done
fi
