#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else

    for arg in "$@"
    do
        line_number=$(grep -n "install -D ../artix-meson -t" "${arg}/PKGBUILD" | cut -d : -f 1)
        HELPER='  # Artix packaging helper
  install -D ../artix-meson -t "${pkgdir}/usr/bin"
  # compat symlink Arch packaging helper
  ln -sfv artix-meson "${pkgdir}"/usr/bin/arch-meson'
        ed - "${arg}/PKGBUILD" << EOF
${line_number} c
${HELPER}
.
w
EOF
        rm "${arg}/arch-meson"
    done
fi
