#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e 's|Arch Pkg|Artix Forum|' \
            -e 's|https://gitlab.archlinux.org/archlinux/packaging/packages/|https://forum.artixlinux.org/|' \
            -i "${arg}/xfce4-smartbookmark-plugin-archlinux.patch"
    done
fi