#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/archlinux/artixlinux/g' "${arg}/PKGBUILD"
        sed -i 's/archlinux/artixlinux/g' "${arg}/plymouth.initcpio_install"
        sed -i 's/artixlinux-logo.png/artixlinux-logo.png -Dsystemd-integration=false/' "${arg}/PKGBUILD"
    done
fi
