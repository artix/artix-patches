#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i "/dbus-broker-units/d" "${arg}/PKGBUILD"
        sed -i "/dbus-units/d" "${arg}/PKGBUILD"
    done
fi
