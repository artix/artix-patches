#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/enable-logind/enable-logind=elogind/' "${arg}/PKGBUILD"
        sed -i 's/configure/configure --without-systemdunitdir/' "${arg}/PKGBUILD"
    done
fi
