#!/bin/bash

# add sysvinit_openrc option to build options in PKGBUILD

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"; do
        sed -e "/python -m build/s/$/ -C--init-system=sysvinit_openrc/" \
	    -i "$arg/PKGBUILD"
    done
fi
