#!/bin/bash

# Comment out every line that contains netplan in the PKGBUILD file

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"; do
        sed -e "s/^.*netplan/#&/g" \
	    -i "$arg/PKGBUILD"
    done
fi
