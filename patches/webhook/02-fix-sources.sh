#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E "s/(source=\(.*$)/\1)/" \
            -i "${arg}/PKGBUILD"
    done
fi

