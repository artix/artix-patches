#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s|build()|prepare() {\n  sed -e \"s/ArchLinux/ArtixLinux/g\" -i \"\${srcdir}/hexchat/plugins/sysinfo/unix/parse.c\"\n}\n\nbuild()|g" \
            -i "${arg}/PKGBUILD"
    done
fi
