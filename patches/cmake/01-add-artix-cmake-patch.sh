#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i 's/signed)/signed/' "${arg}/PKGBUILD"
        sed -i "s/^sha512sums=.*/sha512sums=('SKIP'/" "${arg}/PKGBUILD"
        source_line=$(grep -n "source=" "${arg}/PKGBUILD" | cut -d : -f 1)
        source_line=$((source_line+1))
        sed -i "${source_line}"'i\        artix-cmake.patch)' "${arg}/PKGBUILD"

        sha512_line=$(grep -n "sha512sums=" "${arg}/PKGBUILD" | cut -d : -f 1)
        sha512_line=$((sha512_line+1))
        sed -i "${sha512_line}"'i\            '"'SKIP')" "${arg}/PKGBUILD"

        pgp_line=$(grep -n "validpgpkeys" "${arg}/PKGBUILD" | cut -d : -f 1)
        pgp_line=$((pgp_line+1))
        prepare_block='
prepare() {
  git -C "${pkgname}" apply ../artix-cmake.patch
}
'
        ed - "${arg}/PKGBUILD" << EOF
${pgp_line} c
${prepare_block}
.
w
EOF
    done
fi
