#!/bin/bash

# 01-rm-with-systemd-option.sh
# remove line with '--with-systemd'

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed '/--with-systemd/d' \
            -i "${arg}/PKGBUILD"
    done
fi
