#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E 's|install.*\.service.*$|install -Dm644 "$srcdir/stubby-sysusers.conf" "$pkgdir/usr/lib/sysusers.d/stubby.conf"|' \
            -i "${arg}/PKGBUILD"
    done
fi
