#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e 's|systemd-libs|libudev|' \
            -e "s|\s*'systemd'\s*||" \
            -i "${arg}/PKGBUILD"
    done
fi