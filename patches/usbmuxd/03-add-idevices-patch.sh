#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        source_line=$(grep -n "^source" "${arg}/PKGBUILD" | cut -d : -f 1)
        source_line=$((source_line+1))
        sed -i "${source_line}"'i\  0001-iDevices.patch' "${arg}/PKGBUILD"

        b2_line=$(grep -n "^b2" "${arg}/PKGBUILD" | cut -d : -f 1)
        b2_line=$((b2_line+1))
        sed -i "${b2_line}"'i\        SKIP' "${arg}/PKGBUILD"

        git_line=$(grep -n "git apply" "${arg}/PKGBUILD" | cut -d : -f 1)
        git_line=$((git_line+1))
        sed -i "${git_line}"'i\  git apply -3 ../0001-iDevices.patch' "${arg}/PKGBUILD"
    done
fi

