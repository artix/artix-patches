#!/usr/bin/perl

use strict;
use warnings;

if (@ARGV == 0) {
    die("Usage: $0 <package repo>\n");
}

# Process entire file instead of line by line.
undef $/;

for (@ARGV) {
    my $file_name = $_ . "/PKGBUILD";
    open(FILE,$file_name);
    my $content = <FILE>;
    close(FILE);
    $content =~ s/(cmake (.|\\\n)*-B(.|\\\n)*[^\\])\n/$1 \\\n    -DCMAKE_DISABLE_FIND_PACKAGE_Systemd=ON\n/g;
    open(FILE,">$file_name");
    print FILE $content;
    close(FILE);
}
