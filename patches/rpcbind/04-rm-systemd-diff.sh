#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -i '/udev_service/d' "${arg}/PKGBUILD"
        rm systemd_service.diff
        sed -i 's/udev sysusers/systemd sysusers/' "${arg}/PKGBUILD"
    done
fi
