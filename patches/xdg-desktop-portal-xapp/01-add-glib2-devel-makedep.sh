#!/bin/bash

# 01-add-glib2-devel-makedep.sh
# replace makedep 'meson' with 'meson glib2-devel'

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s/makedepends=(meson)/makedepends=(meson) glib2-devel/" \
            -i "${arg}/PKGBUILD"
    done
fi
