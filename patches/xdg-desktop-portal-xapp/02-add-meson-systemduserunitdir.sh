#!/bin/bash

# 02-add-meson-systemduserunitdir.sh

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -e "s|artix-meson|artix-meson -D systemduserunitdir=/usr/lib/systemd|" \
            -i "${arg}/PKGBUILD"
    done
fi
