#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        line_number=$(grep -n "for _p in hv_kvp_daemon" "${arg}/PKGBUILD" | cut -d : -f 1)
        sed -i "${line_number}"'d' "${arg}/PKGBUILD"
        sed -i "${line_number}"'d' "${arg}/PKGBUILD"
        rm cpupower.systemd
    done
fi
