#!/bin/bash

if [ "$#" -eq 0 ]; then
    echo "Usage: $0 <package repo>"
    exit 1
else
    for arg in "$@"
    do
        sed -E 's|(sip-build)|\1 --scripts-dir=/usr/bin|' \
            -i "${arg}/PKGBUILD"
    done
fi